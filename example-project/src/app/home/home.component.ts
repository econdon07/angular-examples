import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  
  constructor(private router: Router) {
  }

  sayHello(name: string) {
    console.log('Hi', name);
    return 'Hi ' + name;
  }

  onClickNavigateDifferent() {
    this.router.navigate(['/different'], {
      queryParams: {
        id: 6, 
        age: 37
      }
    });
  }
}

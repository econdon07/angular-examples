import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book.model';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css'],
})
export class LibraryComponent implements OnInit {
  books: Array<Book> = [];
  newBook: Book = new Book("", "", "");
  constructor() {}

  ngOnInit(): void {
    this.books = [
      new Book('Learn Angular', 'Paul', '12334'),
      new Book('Learn JS', 'Maaike', '2342342'),
      new Book('Learn C#', '70 people from Romania', '345345'),
    ];
  }

  onAddBookSubmit() {
    this.books.push(this.newBook);
    this.newBook = new Book('', '', '');
  }
}

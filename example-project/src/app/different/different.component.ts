import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-different',
  templateUrl: './different.component.html',
  styleUrls: ['./different.component.css']
})
export class DifferentComponent implements OnInit {

  id!: number;
  age!: number;
  constructor(private activatedRoute: ActivatedRoute) {
    console.dir(activatedRoute);
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params["id"];
      this.age = params["age"];
    });
  }

}

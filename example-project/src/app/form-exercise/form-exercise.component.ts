import { Component } from '@angular/core';
import { Contact } from '../models/contact.model';

@Component({
  selector: 'app-form-exercise',
  templateUrl: './form-exercise.component.html',
  styleUrls: ['./form-exercise.component.css']
})
export class FormExerciseComponent {
  contact: Contact;
  contacts: Array<Contact> = [];

  constructor() {
    this.contact = new Contact("", "", "");
  }

  onAddContactSubmit() {
    this.contacts.push(this.contact);
    console.dir(this.contact);
    this.contact = new Contact("", "", "");
  }
}

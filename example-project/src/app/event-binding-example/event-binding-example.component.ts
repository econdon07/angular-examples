import { Component } from '@angular/core';

@Component({
  selector: 'app-event-binding-example',
  templateUrl: './event-binding-example.component.html',
  styleUrls: ['./event-binding-example.component.css'],
})
export class EventBindingExampleComponent {
  title = 'change-event-demo';
  packageName: string = 'Please select a package';
  location: string = '';
  price: string = '';
  public packages: Array<any>;
  constructor() {
    // load the package list
    this.packages = [
      {
        id: 'christmas',
        name: 'Christmas Family Getaway',
        location: 'Aspen, Colorado',
        price: '$1200 adults/$695 kids',
      },
      {
        id: 'valentines',
        name: "Romantic Valentine's Day Retreat",
        location: 'Charleston, South Carolina',
        price: '$1300 per couple',
      },
      {
        id: 'fourth',
        name: 'July 4th Holiday in Washington DC',
        location: 'Washington, DC',
        price: '$700 adults / $425 kids',
      },
    ];
  }
  onPackageChange($event: any) {
    // get the value of the currently selected package
    let selectedPackage = $event.target.value;
    // if the chose Select one...
    if (selectedPackage == '') {
      this.packageName = 'Please select a package';
      this.location = '';
      this.price = '';
    } else {
      // otherwise find the package
      let matchingPackage = this.packages.find((x) => x.id == selectedPackage);
      this.packageName = matchingPackage.name;
      this.location = matchingPackage.location;
      this.price = matchingPackage.price;
    }
  }
}

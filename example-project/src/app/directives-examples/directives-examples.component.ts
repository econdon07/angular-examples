import {
  Component,
  DoCheck,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-directives-examples',
  templateUrl: './directives-examples.component.html',
  styleUrls: ['./directives-examples.component.css'],
})
export class DirectivesExamplesComponent implements DoCheck {
  classObj = {};
  choice: string = 'yes';
  images = [{ url: 'assets/puppies.jpg' }, { url: 'assets/puppy.jpg' }];

  ngDoCheck() {
    this.classObj = {
      hi: this.choice == 'yes' ? true : false,
      bye: this.choice != 'yes' ? true : false,
    };
  }
}

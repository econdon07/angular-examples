import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-interaction-inner',
  templateUrl: './interaction-inner.component.html',
  styleUrls: ['./interaction-inner.component.css'],
})
export class InteractionInnerComponent {
  @Output() messageEvent = new EventEmitter<string>();
  @Output() saveEvent = new EventEmitter<string>();
  @Input() some: string ="";

  message: string = "";

  onClickSendMessage() {
    console.log(this.some + " sending message: " + this.message);
    this.messageEvent.emit(this.message);
  }

  onClickSave() {
    console.log(this.some + " Will save: " + this.message);
    this.saveEvent.emit(this.message);
  }
}

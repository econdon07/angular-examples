import { Component, ViewEncapsulation, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  title = 'example-project';
  msg = 'Change this!';
  today = new Date();
  footerText = "";

  items = [
    { id: 1, name: 'Item 1', price: 10.99 },
    { id: 2, name: 'Item 2', price: 19.99 },
    { id: 3, name: 'Item 3', price: 4.99 },
    { id: 4, name: 'Item 4', price: 8.99 },
  ];

  filterText = '';
  sortField = 'name';
  sortOrder = 'desc';

  processText($event: any) {
    this.footerText = $event;
  }
  
}

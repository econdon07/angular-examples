import { Component } from '@angular/core';

@Component({
  selector: 'app-data-binding-examples',
  templateUrl: './data-binding-examples.component.html',
  styleUrls: ['./data-binding-examples.component.css']
})
export class DataBindingExamplesComponent {
  inputType: string = "text";
  imgUrl: string = "assets/puppy.jpg";
  imgDesc: string = "this puppy is so cute";
  size: number = 500;
  test: string = "color: red";
  something(str: string) {

  }
}
